package maven;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CucumberCustomer {
	
	WebDriver driver;
	
	@Given("^Access to LegacyCRM$")
	public void AccessLCRM (){
		
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		driver = new InternetExplorerDriver();
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
	}
	
	@When("^Type your username and password$")
	public void TypingLoginInformations(){
	
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiprobaslan");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
	}
	
	@And("^Click login button$")
	public void ClickLoginButton(){
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}
	
	@Then("^Verify that legacycrm is opened$")
	public void legacycrmverification(){
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		boolean vry =driver.findElement(By.xpath("//img[@src='images/vodafone_logo_top.gif']")).isDisplayed();
		if(vry){
			System.out.println("application has been opened");
		}
		else{
			
			System.out.println("something wrong");
		}
		
	}
	@And("^Musteri Olustur tabina tikla ve acilan sayfada submit butonuna tikla$")
	public void Customercreatev1(){
		
		driver.findElement(By.xpath("//a[@title='M��teri Olu�turma']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='Submit']")).click();
	}

}
