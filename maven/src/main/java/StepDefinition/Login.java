package StepDefinition;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Login {
	
	public static void main (String[] args) {
		
	File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
	System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
	WebDriver driver = new InternetExplorerDriver();
	LoginPage (driver);
	UserNameTyping(driver);
	PasswordTyping(driver);
	ClickLogin(driver);
	

	}
	
	@Given("^Log into LegacyCRM page$")
	public static void LoginPage (WebDriver driver){

		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
	}
	
	@When("^typing username info into username textbox$")
	public static void UserNameTyping(WebDriver driver){
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("wiprobaslan");
		
	}

	@And("^typing password into into password textbox$")
	public static void PasswordTyping(WebDriver driver){
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Voda123456789**");
	}
	@Then("^click login button and see that login should be successful$")
	public static void ClickLogin(WebDriver driver){
		WebElement login=driver.findElement(By.xpath("//input[@src='/TelsimGlobal/Menu/image3.gif']"));
		login.click();
	}


}
