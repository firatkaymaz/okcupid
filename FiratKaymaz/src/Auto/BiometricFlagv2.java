package Auto;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.SQLException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BiometricFlagv2 {

	public static void main(String[] args) throws InterruptedException, ClassNotFoundException, SQLException, AWTException {
		// TODO Auto-generated method stub
		//
		BiometricFlag.DBConnection();
		File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
		System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
		WebDriver driver = new InternetExplorerDriver();
		//SECURITY PAGE
		LineActivation.SecurityPage(driver);
		//LOGIN PAGE
		LineActivation.LoginPage(driver);
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467028283");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.cssSelector("//a[href*='301']")).click(); 
		Thread.sleep(1000);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		BiometricFlagforSimCard (driver);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("bx");
		driver.findElement(By.xpath("//input[@value='�IKI�']")).click();
		Thread.sleep(1000);
		driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
		BiometricFlag.DBConnection2();
		LineActivation.LoginPage(driver);
		SimCardV2(driver);
		BiometricFlagforSimCard (driver);
		driver.quit();
		
	}
	
	public static void BiometricFlagforSimCard(WebDriver driver){
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		try{
			driver.findElement(By.xpath("//input[@name='biometric']"));
			System.out.println("Biometric flag is visible for Sim Card Change");
		} catch (org.openqa.selenium.NoSuchElementException e)
		{
			System.out.println("Biometric flag is not visible for Sim Card Change");
		}
				
	}
	
	public static void SimCardV2 (WebDriver driver) throws InterruptedException, AWTException{
		
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu");
		driver.findElement(By.xpath("//a[@title='M��teri-Abone Listesi']")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@name='gsmno']")).sendKeys("5467028283");
		driver.findElement(By.xpath("//input[@name='bul']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Sair i�lem']")).sendKeys(Keys.ENTER);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.cssSelector("//a[href*='301']")).click(); 
		Thread.sleep(1000);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

}
