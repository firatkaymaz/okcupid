package Auto;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class BiometricFlag {

	public static void main(String[] args) throws ClassNotFoundException, SQLException, InterruptedException {
		
    DBConnection();
    File file = new File("C:/Users/wiprofkaymaz/Desktop/ie32/IEDriverServer.exe");
	System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
	WebDriver driver = new InternetExplorerDriver();
	LineActivation.SecurityPage(driver);
	LineActivation.LoginPage(driver);	   
	driver.switchTo().frame("ax");
	driver.switchTo().frame("menu").findElement(By.xpath("//a[@title='A K T � V A S Y O N']")).click();
	LineActivation.RoadtoActivation(driver);
	String mainWindow = driver.getWindowHandle();
	LineActivation.Windows(driver, mainWindow);
	LineActivation.SecuritPageCheck(driver);
	driver.switchTo().window(mainWindow);
	Thread.sleep(1000);
	driver.switchTo().defaultContent();
	driver.switchTo().frame("cx");
	driver.findElement(By.xpath("//input[@value='Devam']")).click();
	Thread.sleep(1000);
	driver.switchTo().alert().accept();
	BiometricFlagforActivation(driver);
	Thread.sleep(1000);
	driver.switchTo().defaultContent();
	driver.switchTo().frame("bx");
	driver.findElement(By.xpath("//input[@value='�IKI�']")).click();
	Thread.sleep(1000);
	driver.get("https://iccbcf/TelsimGlobal/Menu/showLogin.jsp");
	DBConnection2();
	LineActivation.LoginPage(driver);
	CheckingAgain(driver);
	BiometricFlagforActivation(driver);
	}

	public static void DBConnection() throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   System.out.println("DB Connection is ok");
		   Statement stmt = conn.createStatement();
		   stmt.executeQuery("update ccb.CCB_COMMON_PARAMETER_NUMBER set text_value='K000003,S900005,' where parameter_code='BIOMETRIC_SHOP_ID_ACCESS'");
	}
	
	public static void DBConnection2() throws ClassNotFoundException, SQLException{
		
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   Statement stmt = conn.createStatement();
		   stmt.executeQuery("update ccb.CCB_COMMON_PARAMETER_NUMBER set text_value='K000003,' where parameter_code='BIOMETRIC_SHOP_ID_ACCESS'");
	}
	
	public static void BiometricFlagforActivation (WebDriver driver){
		
		LineActivation.switchwindow2(driver, "cx", "frame1");
		try{
			driver.findElement(By.xpath("//input[@name='biometric']"));
			System.out.println("Biometric flag is visible for line activation");
		} catch (org.openqa.selenium.NoSuchElementException e)
		{
			System.out.println("Biometric flag is not visible for line activation");
		}
				
	}
	public static void CheckingAgain (WebDriver driver) throws InterruptedException{
		driver.switchTo().frame("ax");
		driver.switchTo().frame("menu").findElement(By.xpath("//a[@title='A K T � V A S Y O N']")).click();
		LineActivation.RoadtoActivation(driver);
		String mainWindow = driver.getWindowHandle();
		LineActivation.Windows(driver, mainWindow);
		LineActivation.SecuritPageCheck(driver);
		driver.switchTo().window(mainWindow);
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("cx");
		driver.findElement(By.xpath("//input[@value='Devam']")).click();
		Thread.sleep(1000);
		driver.switchTo().alert().accept();
	}
}
