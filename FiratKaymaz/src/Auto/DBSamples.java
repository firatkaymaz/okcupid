package Auto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBSamples {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		 Class.forName("oracle.jdbc.OracleDriver");
		   Connection conn =  DriverManager.getConnection("jdbc:oracle:thin:@10.86.143.27:1556:CFICCB", "TESTUSER","TESTUSER");
		   Statement stmt = conn.createStatement();
		   stmt.executeQuery("select * from ccb.ccb_icci_sim_pool WHERE USAGE_REASON_CODE = 'YD' AND icci IN (SELECT icci FROM ccb.ccb_imsi_pool WHERE gsm_no IS NULL and brand_code='MC' and imsi_status_code='PS') AND CARD_TYPE = 'NO' AND ICCI_STATUS_CODE = 'BA' AND LOCK_IND = 'H' AND CAMPAIGN_CODE='DEF_MC'");
		   ResultSet rs = stmt.getResultSet();
		   if(rs.next()){
			   String s = rs.getString(1);
			   System.out.println(s);
		   }
	}

}
